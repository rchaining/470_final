Group:
Robert Nichols-Haining (rhaining1@mix.wvu.edu, rchaining@gmail.com)
Benjamin Weimer (bweimer@mix.wvu.edu)
Veronica Blackshire (vblacksh@mix.wvu.edu)

Environment Tested On:
Ubuntu 16.04
In virtualbox

To compile:
Navigate to project directory in terminal
execute commands:
  cmake .
  make
  bin/draw_scene
  
This will launch the scene in a windows.

NOTE: It looks like the model loader might have misinterpreted the obj file? Though I accept the possibility that I just messeed something up.
regardless, it still creates a valid model object, so I didn't bother with it. There are two weird rotating messes, and a 3rd stationary one.
