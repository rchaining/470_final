# ifndef HELPER_FUNCS_CC
# define HELPER_FUNCS_CC

#include <cmath>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
// The header of GLFW. This library is a C-based and light-weight library for
// creating windows for OpenGL rendering.
// See http://www.glfw.org/ for more information.
#include <GLFW/glfw3.h>

#include "model.h"
#include "shader_program.h"

#define _USE_MATH_DEFINES  // For using M_PI.
#include "HelperFunctions.h"

// Include CImg library to load textures.
// The macro below disables the capabilities of displaying images in CImg.
#define cimg_display 0
#include <CImg.h>

// -------------------- Helper Functions ----------------------------------
Eigen::Matrix4f ComputeTranslation(
  const Eigen::Vector3f& offset) {
  Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();
  transformation.col(3) = offset.homogeneous();
  return transformation;
}

Eigen::Matrix4f ComputeRotation(const Eigen::Vector3f& axis,
                                const GLfloat angle) {
  Eigen::AngleAxisf rotation(angle, axis);
  Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();
  Eigen::Matrix3f rot3 = rotation.matrix();
  transformation.block(0, 0, 3, 3)  = rot3;
  return transformation;
}

// General form.
Eigen::Matrix4f ComputeProjectionMatrix(
  const GLfloat left, 
  const GLfloat right, 
  const GLfloat top, 
  const GLfloat bottom, 
  const GLfloat near, 
  const GLfloat far) {
  Eigen::Matrix4f projection = Eigen::Matrix4f::Identity();
  projection(0, 0) = 2.0 * near / (right - left);
  projection(1, 1) = 2.0 * near / (top - bottom);
  projection(2, 2) = -(far + near) / (far - near);
  projection(0, 2) = (right + left) / (right - left);
  projection(1, 2) = (top + bottom) / (top - bottom);
  projection(2, 3) = -2.0 * far * near / (far - near);
  projection(3, 3) = 0.0f;
  projection(3, 2) = -1.0f;
  return projection;
}

// Mathematical constants. The right way to get PI in C++ is to use the
// macro M_PI. To do so, we have to include math.h or cmath and define
// the _USE_MATH_DEFINES macro to enable these constants. See the header
// section.
constexpr GLfloat kHalfPi = 0.5f * static_cast<GLfloat>(M_PI);

// Compute cotangent. Since C++ does not provide cotangent, we implement it
// as follows. Recall that cotangent is essentially tangent flipped and
// translated 90 degrees (or PI / 2 radians). To do the flipping and translation
// we have to do PI / 2 - angle. Subtracting the angle flips the curve.
// See the plots for http://mathworld.wolfram.com/Cotangent.html and
// http://mathworld.wolfram.com/Tangent.html
inline GLfloat ComputeCotangent(const GLfloat angle) {
  return tan(kHalfPi - angle);
}

// Reparametrization of the ComputeProjectionMatrix. This function only
// requires 4 parameters rather than 6 parameters.
Eigen::Matrix4f ComputeProjectionMatrix(const GLfloat field_of_view,
                                        const GLfloat aspect_ratio,
                                        const GLfloat near,
                                        const GLfloat far) {
  // Create the projection matrix.
  const GLfloat y_scale = ComputeCotangent(0.5f * field_of_view);
  const GLfloat x_scale = y_scale / aspect_ratio;
  const GLfloat planes_distance = far - near;
  const GLfloat z_scale =
      -(near + far) / planes_distance;
  const GLfloat homogeneous_scale =
      -2 * near * far / planes_distance;
  Eigen::Matrix4f projection_matrix;
  projection_matrix << x_scale, 0.0f, 0.0f, 0.0f,
      0.0f, y_scale, 0.0f, 0.0f,
      0.0f, 0.0f, z_scale, homogeneous_scale,
      0.0f, 0.0f, -1.0f, 0.0f;
  return projection_matrix;
}

// -------------------- Texture helper functions -------------------------------
GLuint LoadTexture(const std::string& texture_filepath) {
  cimg_library::CImg<unsigned char> image;
  image.load(texture_filepath.c_str());
  const int width = image.width();
  const int height = image.height();
  // OpenGL expects to have the pixel values interleaved (e.g., RGBD, ...). CImg
  // flatens out the planes. To have them interleaved, CImg has to re-arrange
  // the values.
  // Also, OpenGL has the y-axis of the texture flipped.
  image.permute_axes("cxyz");
  GLuint texture_id;
  glGenTextures(1, &texture_id);
  glBindTexture(GL_TEXTURE_2D, texture_id);
  // We are configuring texture wrapper, each per dimension,s:x, t:y.
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  // Define the interpolation behavior for this texture.
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  /// Sending the texture information to the GPU.
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGB, GL_UNSIGNED_BYTE, image.data());
  // Generate a mipmap.
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  return texture_id;
}


// -------------------- GLFW helper functions -------------------------------
void SetWindowHints() {
  // Sets properties of windows and have to be set before creation.
  // GLFW_CONTEXT_VERSION_{MAJOR|MINOR} sets the minimum OpenGL API version
  // that this program will use.
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  // Sets the OpenGL profile.
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  // Sets the property of resizability of a window.
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

// Configures the view port.
// Note: All the OpenGL functions begin with gl, and all the GLFW functions
// begin with glfw. This is because they are C-functions -- C does not have
// namespaces.
void ConfigureViewPort(GLFWwindow* window) {
  int width;
  int height;
  // We get the frame buffer dimensions and store them in width and height.
  glfwGetFramebufferSize(window, &width, &height);
  // Tells OpenGL the dimensions of the window and we specify the coordinates
  // of the lower left corner.
  glViewport(0, 0, width, height);
}

// Clears the frame buffer.
void ClearTheFrameBuffer() {
  // Sets the initial color of the framebuffer in the RGBA, R = Red, G = Green,
  // B = Blue, and A = alpha.
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  // Tells OpenGL to clear the Color buffer.
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


// -------------------- EBO/VBO/VAO helper functions -------------------------------

GLuint SetElementBufferObject(const Model& model) {
  // Creating element buffer object (EBO).
  GLuint element_buffer_object_id;
  glGenBuffers(1, &element_buffer_object_id);
  // Set the created EBO as current.
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_object_id);
  const std::vector<GLuint>& indices = model.indices();
  const int indices_size_in_bytes = indices.size() * sizeof(indices[0]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               indices_size_in_bytes,
               indices.data(),
               GL_STATIC_DRAW);
  // NOTE: Do not unbing EBO. It turns out that when we create a buffer of type
  // GL_ELEMENT_ARRAY_BUFFER, the VAO who contains the EBO remembers the
  // bindings we perform. Thus if we unbind it, we detach the created EBO and we
  // won't see results.
  return element_buffer_object_id;
}

// Creates and transfers the vertices into the GPU. Returns the vertex buffer
// object id.
GLuint SetVertexBufferObject(const Model& model) {
  // Create a vertex buffer object (VBO).
  GLuint vertex_buffer_object_id;
  glGenBuffers(1, &vertex_buffer_object_id);
  // Set the GL_ARRAY_BUFFER of OpenGL to the vbo we just created.
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_id);
  // Copy the vertices into the GL_ARRAY_BUFFER that currently 'points' to our
  // recently created vbo. In this case, sizeof(vertices) returns the size of
  // the array vertices (defined above) in bytes.
  // First parameter specifies the destination buffer.
  // Second parameter specifies the size of the buffer.
  // Third parameter specifies the pointer to the vertices buffer in RAM.
  // Fourth parameter specifies the way we want OpenGL to treat the buffer.
  // There are three different ways to treat this buffer:
  // 1. GL_STATIC_DRAW: the data will change very rarely.
  // 2. GL_DYNAMIC_DRAW: the data will likely change.
  // 3. GL_STREAM_DRAW: the data will change every time it is drawn.
  // See https://www.opengl.org/sdk/docs/man/html/glBufferData.xhtml.
  const Eigen::MatrixXf& vertices = model.vertices();
  const int vertices_size_in_bytes =
      vertices.rows() * vertices.cols() * sizeof(vertices(0, 0));
  glBufferData(GL_ARRAY_BUFFER,
               vertices_size_in_bytes,
               vertices.data(),
               GL_STATIC_DRAW);
  // Inform OpenGL how the vertex buffer is arranged.
  constexpr GLuint kIndex = 0;  // Index of the first buffer array.
  // A vertex right now contains 3 elements because we have x, y, z. But we can
  // add more information per vertex as we will see shortly.
  constexpr GLuint kNumElementsPerVertex = 3;
  constexpr GLuint kStride = 8 * sizeof(vertices(0, 0));
  const GLvoid* offset_ptr = nullptr;
  glVertexAttribPointer(kIndex, kNumElementsPerVertex, 
                        GL_FLOAT, GL_FALSE,
                        kStride, offset_ptr);
  // Set as active our newly generated VBO.
  glEnableVertexAttribArray(kIndex);
  const GLvoid* offset_color = reinterpret_cast<GLvoid*>(3 * sizeof(vertices(0, 0)));
  glVertexAttribPointer(1, kNumElementsPerVertex, 
                        GL_FLOAT, GL_FALSE,
                        kStride, offset_color);
  glEnableVertexAttribArray(1);
  // Configure the texels.
  const GLvoid* offset_texel = 
    reinterpret_cast<GLvoid*>(6 * sizeof(vertices(0, 0)));
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                        kStride, offset_texel);
  glEnableVertexAttribArray(2);
  // Unbind buffer so that later we can use it.
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return vertex_buffer_object_id;
}

// Creates and sets the vertex array object (VAO) for our triangle. Returns the
// id of the created VAO.
void SetVertexArrayObject(const Model& model,
                          GLuint* vertex_buffer_object_id,
                          GLuint* vertex_array_object_id,
                          GLuint* element_buffer_object_id) {
  // Create the vertex array object (VAO).
  constexpr int kNumVertexArrays = 1;
  // This function creates kNumVertexArrays vaos and stores the ids in the
  // array pointed by the second argument.
  glGenVertexArrays(kNumVertexArrays, vertex_array_object_id);
  // Set the recently created vertex array object (VAO) current.
  glBindVertexArray(*vertex_array_object_id);
  // Create the Vertex Buffer Object (VBO).
  *vertex_buffer_object_id = SetVertexBufferObject(model);
  *element_buffer_object_id = SetElementBufferObject(model);
  // Disable our created VAO.
  glBindVertexArray(0);
}


// -------------------- Shader helper functions -------------------------------
    
bool CreateShaderProgram(wvu::ShaderProgram* shader_program) {
  if (shader_program == nullptr) return false;
  shader_program->LoadVertexShaderFromFile("vertex_shader.glsl");
  shader_program->LoadFragmentShaderFromFile("fragment_shader.glsl");
  std::string error_info_log;
  if (!shader_program->Create(&error_info_log)) {
    std::cout << "ERROR: " << error_info_log << "\n";
  }
  if (!shader_program->shader_program_id()) {
    std::cerr << "ERROR: Could not create a shader program.\n";
    return false;
  }
  return true;
}
# endif
