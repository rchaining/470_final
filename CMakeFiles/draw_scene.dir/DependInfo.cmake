# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/christian/Desktop/finalproj/final_project/HelperFunctions.cc" "/home/christian/Desktop/finalproj/final_project/CMakeFiles/draw_scene.dir/HelperFunctions.cc.o"
  "/home/christian/Desktop/finalproj/final_project/draw_scene.cc" "/home/christian/Desktop/finalproj/final_project/CMakeFiles/draw_scene.dir/draw_scene.cc.o"
  "/home/christian/Desktop/finalproj/final_project/model_loader.cc" "/home/christian/Desktop/finalproj/final_project/CMakeFiles/draw_scene.dir/model_loader.cc.o"
  "/home/christian/Desktop/finalproj/final_project/shader_program.cc" "/home/christian/Desktop/finalproj/final_project/CMakeFiles/draw_scene.dir/shader_program.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "/usr/include/eigen3"
  "libraries/glfw-3.1.2/include"
  "libraries/cimg"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/christian/Desktop/finalproj/final_project/libraries/glfw-3.1.2/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
