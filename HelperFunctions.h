# ifndef HELPER_FUNCS_H
# define HELPER_FUNCS_H

#include "model.h"
class Model;

Eigen::Matrix4f ComputeTranslation(const Eigen::Vector3f&);
  
Eigen::Matrix4f ComputeRotation(const Eigen::Vector3f&,
                                const GLfloat);
                                
Eigen::Matrix4f ComputeProjectionMatrix(
  const GLfloat left, 
  const GLfloat right, 
  const GLfloat top, 
  const GLfloat bottom, 
  const GLfloat near, 
  const GLfloat far);
  
Eigen::Matrix4f ComputeProjectionMatrix(const GLfloat field_of_view, // Reparameratization of above
                                        const GLfloat aspect_ratio,
                                        const GLfloat near,
                                        const GLfloat far);
                                        
GLuint LoadTexture(const std::string& texture_filepath);

void SetWindowHints();

void ConfigureViewPort(GLFWwindow* window);

void ClearTheFrameBuffer();

GLuint SetElementBufferObject(const Model& model);

GLuint SetVertexBufferObject(const Model& model);

void SetVertexArrayObject(const Model& model,
                          GLuint* vertex_buffer_object_id,
                          GLuint* vertex_array_object_id,
                          GLuint* element_buffer_object_id);
                  
bool CreateShaderProgram(wvu::ShaderProgram* shader_program);
        
# endif
