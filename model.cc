#include "model.h"

Model::Model(const Eigen::Vector3f& orientation,
      const Eigen::Vector3f& position,
      const Eigen::MatrixXf& vertices,
      const std::vector<GLuint>& indices,
      const GLuint texture_id);

// Sets the VAO, VBO and EBO.
void Model::SetVerticesIntoGpu();
void Model::getIDs (GLuint * vbo_id, GLuint * vao_id, GLuint * ebo_id, GLuint * tex_id);


