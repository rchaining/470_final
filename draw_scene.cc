// Copyright (C) 2016 West Virginia University.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of West Virginia University nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// TODO: Add your names and emails using the following format:
// Author: Victor Fragoso (victor.fragoso@mail.wvu.edu)

// TODO: Include the headers you need for your project.

#include <iostream>
#define GLEW_STATIC
#include <GL/glew.h>
// The header of GLFW. This library is a C-based and light-weight library for
// creating windows for OpenGL rendering.
// See http://www.glfw.org/ for more information.
#include <GLFW/glfw3.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "model.h"
#include "shader_program.h"
#include "model_loader.h"
#include "HelperFunctions.h"

// Use the right namespace for google flags (gflags).
#ifdef GFLAGS_NAMESPACE_GOOGLE
#define GLUTILS_GFLAGS_NAMESPACE google
#else
#define GLUTILS_GFLAGS_NAMESPACE gflags
#endif
#include <gflags/gflags.h>
#include <glog/logging.h>

// Include library headers.
// Include CImg library to load textures.
// The macro below disables the capabilities of displaying images in CImg.
#define cimg_display 0
#include <CImg.h>

// Google flags.
// (<name of the flag>, <default value>, <Brief description of flat>)
// These will define global variables w/ the following format
// FLAGS_vertex_shader_filepath and 
// FLAGS_fragment_shader_filepath.
// DEFINE_<type>(name of flag, default value, brief description.)
// types: string, int32, bool.
DEFINE_string(vertex_shader_filepath, "", 
              "vertex_shader.glsl");
DEFINE_string(fragment_shader_filepath, "",
              "fragment_shader.glsl");
DEFINE_string(texture_filepath, "", "Filepath of the texture.");


// Window dimensions.
constexpr int kWindowWidth = 640;
constexpr int kWindowHeight = 480;

// Error callback function. This function follows the required signature of
// GLFW. See http://www.glfw.org/docs/3.0/group__error.html for more
// information.
static void ErrorCallback(int error, const char* description) {
  std::cerr << "ERROR: " << description << std::endl;
}

// Key callback. This function follows the required signature of GLFW. See
// http://www.glfw.org/docs/latest/input_guide.html fore more information.
static void KeyCallback(GLFWwindow* window,
                        int key,
                        int scancode,
                        int action,
                        int mods) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }
}

// Check/print errors
void ErrorReport(GLenum err){
  switch(err){
  case GL_NO_ERROR:
    return;
  case GL_INVALID_ENUM:
    std::cout << "INVALID ENUM\n";
    break;
  case GL_INVALID_VALUE:
    std::cout << "INVALID VALUE\n";
    exit(-1);
    break;
  case GL_INVALID_OPERATION:
    std::cout << "INVALID OP\n";
    exit(-1);
    break;
  case GL_STACK_OVERFLOW:
    std::cout << "STACK OVERFLOW\n";
    exit(-1);
    break;
  case GL_STACK_UNDERFLOW:
    std::cout << "STACK UNDERFLOW\n";
    break;
  case GL_OUT_OF_MEMORY:
    std::cout << "OUT OF MEMORY\n";
    exit(-1);
    break;
  case GL_INVALID_FRAMEBUFFER_OPERATION:
    std::cout << "INVALID FRAMEBUFF OPERATION\n";
    exit(-1);
    break;
  case GL_CONTEXT_LOST:
    std::cout << "CONTEXT LOST\n";
    exit(-1);
    break;
  case GL_TABLE_TOO_LARGE:
    std::cout << "TABLE TOO LARGE\n";
    exit(-1);
    break;
  default:
    std::cout << "ERROR UNDEFINED\n";
    exit(-1);
  }
}

void VocalErrorReport(GLenum err){
  static int i=0;
  std::cout << "\n" << i << " Error checking: ";
  ErrorReport(err);
  i++;
}

// Renders the scene.
void RenderScene(const wvu::ShaderProgram& shader_program,
                 const Eigen::Matrix4f& projection,
                 const GLfloat angle,
                 GLFWwindow* window,
                 std::vector<Model *> models) {
  GLuint vao;
  GLuint tex;
  for(int i=0; i<models.size(); i++){
    //models[i]->getIDs(&vbo, &vao, &ebo, &tex);
    vao = models[i]->getVAO();
    tex = models[i]->getTex();
    // Translation
    Eigen::Matrix4f translation = 
      ComputeTranslation(models[i]->GetPosition());
    // Rotation
    Eigen::Matrix4f rotation;
    if(models[i]->no_rotate){
      rotation = ComputeRotation(Eigen::Vector3f(0.0, 0.0, 0.0), 45.0f * M_PI/180.0f);
    }
    else{
      rotation = ComputeRotation(Eigen::Vector3f(0.0, 1.0, 0.0f).normalized(), angle);
    }
    
    Eigen::Matrix4f model = translation*rotation;
    Eigen::Matrix4f view = Eigen::Matrix4f::Identity();
    
    const GLint model_location = 
      glGetUniformLocation(shader_program.shader_program_id(), "model");
    const GLint view_location = 
      glGetUniformLocation(shader_program.shader_program_id(), "view");
    const GLint projection_location = 
      glGetUniformLocation(shader_program.shader_program_id(), "projection");
    
    //const GLint vertex_color_location =
    //  glGetUniformLocation(shader_program.shader_program_id(), "vertex_color");
    //std::cout << "\n\n After uniform accesses?"; ErrorReport(glGetError());
    // When variable is not found you get a - 1.
    glBindTexture(GL_TEXTURE_2D, tex);
    
    //const Eigen::Vector4f color(0.5f, 0.5f, 0.5f, 1.0f);
    //glUniform4fv(vertex_color_location, 1, color.data());
    
    glUniformMatrix4fv(model_location, 1, GL_FALSE, model.data());
    glUniformMatrix4fv(view_location, 1, GL_FALSE, view.data());
    glUniformMatrix4fv(projection_location, 1, GL_FALSE, projection.data()); 
    
    //GLfloat color_scalar = static_cast<GLfloat>(glfwGetTime());
    
    
    // Draw
    // Let ogl know what v arr obj we use
    glBindVertexArray(vao);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    /*
      Arg1: mode
      Arg2: Count
      Arg3: Type
      Arg4: Indices
    */
    glDrawElements(GL_TRIANGLES, models[i]->IndicesCount(), GL_UNSIGNED_INT, 0);
    
    //std::cout << models[i]->IndicesCount() << std::endl;
    // Done w/ our vao
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    ErrorReport(glGetError());
  }
  /*Eigen::Matrix4f translation = 
    ComputeTranslation(Eigen::Vector3f(0.0f, 0.0f, -5.0f));
  Eigen::Matrix4f rotation = 
      ComputeRotation(Eigen::Vector3f(0.0, 1.0, 0.0f).normalized(), angle);
  Eigen::Matrix4f model = translation * rotation;
  std::cout << "Model: \n" << model << std::endl;
  Eigen::Matrix4f view = Eigen::Matrix4f::Identity();
  // Bind texture.
  glBindTexture(GL_TEXTURE_2D, texture_id);
  // We do not create the projection matrix here because the projection 
  // matrix does not change.
  // Eigen::Matrix4f projection = Eigen::Matrix4f::Identity();
  glUniformMatrix4fv(model_location, 1, GL_FALSE, model.data());
  glUniformMatrix4fv(view_location, 1, GL_FALSE, view.data());
  glUniformMatrix4fv(projection_location, 1, GL_FALSE, projection.data());
  GLfloat color_scalar = static_cast<GLfloat>(glfwGetTime());
  // Draw the triangle.
  // Let OpenGL know what vertex array object we will use.
  glBindVertexArray(vertex_array_object_id);
    // Set to GL_LINE instead of GL_FILL to visualize the poligons as wireframes.
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  // First argument specifies the primitive to use.
  // Second argument specifies the starting index in the VAO.
  // Third argument specified the number of vertices to use.
  // glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);

  // Using EBOs.
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  
  // Let OpenGL know that we are done with our vertex array object.
  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);*/
} // RenderScene

int main(int argc, char** argv) {
  // TODO: Implement the logic here to draw your scene.
  
  GLUTILS_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);
  // Initialize the GLFW library.
  if (!glfwInit()) {
    return -1;
  }
  std::cout << "\nglfw init error check: ";
  VocalErrorReport(glGetError());
  // Setting the error callback.
  glfwSetErrorCallback(ErrorCallback);

  // Setting Window hints.
  SetWindowHints();

  // Create a window and its OpenGL context.
  const std::string window_name = "Hello Triangle";
  GLFWwindow* window = glfwCreateWindow(kWindowWidth,
                                        kWindowHeight,
                                        window_name.c_str(),
                                        nullptr,
                                        nullptr);
  if (!window) {
    glfwTerminate();
    return -1;
  }
  VocalErrorReport(glGetError());
  // Make the window's context current.
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  glfwSetKeyCallback(window, KeyCallback);

  // Initialize GLEW.
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Glew did not initialize properly!" << std::endl;
    glfwTerminate();
    return -1;
  }
  VocalErrorReport(glGetError());
  // Configure View Port.
  ConfigureViewPort(window);
  /*
  // Compile shaders and create shader program.
  // This is how we access the flags.
  const std::string vertex_shader_filepath = 
    FLAGS_vertex_shader_filepath;
  const std::string fragment_shader_filepath =
    FLAGS_fragment_shader_filepath;
  wvu::ShaderProgram shader_program;
  std::cout << vertex_shader_filepath << std::endl;
  std::cout << fragment_shader_filepath << std::endl;
  shader_program.LoadVertexShaderFromString(vertex_shader_filepath);
  shader_program.LoadFragmentShaderFrom(fragment_shader_filepath);
  std::string error_info_log;
  if (!shader_program.Create(&error_info_log)) {
    std::cout << "ERROR: " << error_info_log << "\n";
  }*/
  
  wvu::ShaderProgram shader_program;

  if (!CreateShaderProgram(&shader_program)) {
    std::cerr << "ERROR: Could not create a shader program.\n";
    return -1;
  }
  VocalErrorReport(glGetError());
  std::vector<Model*> models_to_draw;
  GLuint texture_id = LoadTexture("Background17.jpg");
  // Load Models
  // model 1
  std::vector<Eigen::Vector3f> model_vertices_1;
  std::vector<Eigen::Vector2f> model_texels_1;
  std::vector<Eigen::Vector3f> model_normals_1;
  std::vector<wvu::Face> model_faces_1;
 
  if(!wvu::LoadObjModel("bocalPoisson.obj",
                        &model_vertices_1,
                        &model_texels_1,
                        &model_normals_1,
                        &model_faces_1)) {
    LOG(ERROR) << "Could not load model: bocalPoisson.obj\n";
    return -1;
  }
  else {
    LOG(INFO) << "Model succesfully loaded! \n" << " Num. verices=" << model_vertices_1.size() << "\nNum. triangles=" << model_faces_1.size();
  }
  std::vector<GLuint> indices_1;
  for (int face_id = 0; face_id < model_faces_1.size(); ++face_id) {
    const wvu::Face& face = model_faces_1[face_id];
    indices_1.push_back(face.vertex_indices[0]);
    indices_1.push_back(face.vertex_indices[1]);
    indices_1.push_back(face.vertex_indices[2]);
  }
  Eigen::MatrixXf vertices_1(3, model_vertices_1.size());
  for (int col = 0; col < model_vertices_1.size(); ++col) {
    vertices_1.col(col) = model_vertices_1[col];
  }
  models_to_draw.push_back(new Model(Eigen::Vector3f(0, 0, 0), // Orientation
                                    Eigen::Vector3f(2.0, 3.0, -5.0), // Position
                                    vertices_1,
                                    indices_1,
                                    texture_id));
  //std::cout << "\nt_id_1: " << texture_id_1 << "\n";
  // model 2
  std::vector<Eigen::Vector3f> model_vertices_2;
  std::vector<Eigen::Vector2f> model_texels_2;
  std::vector<Eigen::Vector3f> model_normals_2;
  std::vector<wvu::Face> model_faces_2;
  if(!wvu::LoadObjModel("poubelleInox.obj",
                        &model_vertices_2,
                        &model_texels_2,
                        &model_normals_2,
                        &model_faces_1)) {
    LOG(ERROR) << "\nCould not load model: poubelleInox.obj";
    return -1;
  }
  else {
    LOG(INFO) << "\nModel succesfully loaded! \n" << " Num. verices=" << model_vertices_2.size() << "\nNum. triangles=" << model_faces_2.size();
  }
  std::vector<GLuint> indices_2;
  for (int face_id = 0; face_id < model_faces_1.size(); ++face_id) {
    const wvu::Face& face = model_faces_1[face_id];
    indices_2.push_back(face.vertex_indices[0]);
    indices_2.push_back(face.vertex_indices[1]);
    indices_2.push_back(face.vertex_indices[2]);
  }
  Eigen::MatrixXf vertices_2(3, model_vertices_1.size());
  for (int col = 0; col < model_vertices_1.size(); ++col) {
    vertices_2.col(col) = model_vertices_1[col];
  }
  models_to_draw.push_back(new Model(Eigen::Vector3f(0, 0, 0), // Orientation
                                    Eigen::Vector3f(-2, 0, -10.0), // Position
                                    vertices_2,
                                    indices_2,
                                    texture_id)); // Same texture for both
                                    
  // Model 3
  Eigen::MatrixXf vertices(8, 4);
  // Vertex 0.
  vertices.block(0, 0, 3, 1) = Eigen::Vector3f(0.0f, 1.0f, 0.0f);
  vertices.block(3, 0, 3, 1) = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
  vertices.block(6, 0, 2, 1) = Eigen::Vector2f(0, 0);
  // Vertex 1.
  vertices.block(0, 1, 3, 1) = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
  vertices.block(3, 1, 3, 1) = Eigen::Vector3f(0.0f, 1.0f, 0.0f);
  vertices.block(6, 1, 2, 1) = Eigen::Vector2f(0, 1);
  // Vertex 2.
  vertices.block(0, 2, 3, 1) = Eigen::Vector3f(1.0f, 1.0f, 0.0f);
  vertices.block(3, 2, 3, 1) = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
  vertices.block(6, 2, 2, 1) = Eigen::Vector2f(1, 0);
  // Vertex 3.
  vertices.block(0, 3, 3, 1) = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
  vertices.block(3, 3, 3, 1) = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
  vertices.block(6, 3, 2, 1) = Eigen::Vector2f(1, 1);
  std::vector<GLuint> indices = {
    0, 1, 3,  // First triangle.
    0, 3, 2,  // Second triangle.
  };
  models_to_draw.push_back(new Model(Eigen::Vector3f(0, 0, 0),  // Orientation of object.
                                     Eigen::Vector3f(0, -0.50, -1),  // Position of object.
                                     vertices,
                                     indices,
                                     texture_id));
  models_to_draw[2]->no_rotate = true;
  std::vector<Eigen::Vector3f> model_vertices_3;
  std::vector<Eigen::Vector2f> model;
  for(int i=0; i<models_to_draw.size(); i++)
    models_to_draw[i]->SetVerticesIntoGpu();
  // Create projection matrix.
  const GLfloat field_of_view = M_PI * 45.0f / 180.0f;
  const GLfloat aspect_ratio = kWindowWidth / kWindowHeight;
  const Eigen::Matrix4f projection_matrix = 
      ComputeProjectionMatrix(field_of_view, aspect_ratio, 0.1, 10);
  GLfloat angle = 0.0f;  // State of rotation.
  //VocalErrorReport(glGetError());
  // Loop until the user closes the window.
  const GLfloat rotation_speed = 50.0f;
  
  while (!glfwWindowShouldClose(window)) {
    // Render the scene!
    // Casting using (<type>) -- which is the C way -- is not recommended.
    // Instead, use static_cast<type>(input argument).
    
    // Clear the buffer.
    ClearTheFrameBuffer();
    // Let OpenGL know that we want to use our shader program.
    shader_program.Use();
    
    angle = rotation_speed * static_cast<GLfloat>(glfwGetTime()) * M_PI / 180.f;
    //VocalErrorReport(glGetError());
    RenderScene(shader_program, projection_matrix, angle, window, models_to_draw);

    // Swap front and back buffers.
    glfwSwapBuffers(window);

    // Poll for and process events.
    glfwPollEvents();
  }

  // Cleaning up tasks.
  for(int i=0; i<models_to_draw.size(); i++){
    models_to_draw[i]->clean_up_after_yourself();
  }
  // Destroy window.
  glfwDestroyWindow(window);
  // Tear down GLFW library.
  glfwTerminate();

return 0;
} // Main

