#ifndef MODEL_H
#define MODEL_H

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <GL/glew.h>

#include "shader_program.h"
#include "HelperFunctions.h"


class Model {
public:
  // Constructor.
  // Params
  //  orientation  Axis of rotation whose norm is the angle
  //     (aka Rodrigues vector).
  //  position  The position of the object in the world.
  //  vertices  The vertices forming the object.
  //  indices  The sequence indicating how to use the vertices.
  Model(const Eigen::Vector3f& orientation,
        const Eigen::Vector3f& position,
        const Eigen::MatrixXf& vertices,
        const std::vector<GLuint>& indices,
        const GLuint texture_id) {
    orientation_ = orientation;
    position_ = position;
    vertices_ = vertices;
    indices_ = indices;
    texture_id_ = texture_id;
    no_rotate = false;
    //std::cout << "Model's Tex: " << texture_id_ << "\n\n";
  }

  // Constructor.
  // Params
  //  orientation  Axis of rotation whose norm is the angle
  //     (aka Rodrigues vector).
  //  position  The position of the object in the world.
  //  vertices  The vertices forming the object.
  Model(const Eigen::Vector3f& orientation,
        const Eigen::Vector3f& position,
        const Eigen::MatrixXf& vertices) {
    orientation_ = orientation;
    position_ = position;
    vertices_ = vertices;
  }
  // Default destructor.
  ~Model() {}

  // Setters set members by *copying* input parameters.
  void SetOrientation(const Eigen::Vector3f& orientation) {
    orientation_ = orientation;
  }

  void SetPosition(const Eigen::Vector3f& position){
    position_ = position;
  }

  GLsizei IndicesCount(){  
    return indices_.size();
  }
  
  // If we want to avoid copying, we can return a pointer to
  // the member. Note that making public the attributes work
  // if we want to modify directly the members. However, this
  // is a matter of design.
  Eigen::Vector3f* mutable_orientation() {
    return &orientation_;
  }

  Eigen::Vector3f* mutable_position() {
    return &position_;
  }

  // Getters, return a const reference to the member.
  const Eigen::Vector3f& GetOrientation() {
    return orientation_;
  }
  
  void SetOrientation(Eigen::Vector3f orientation) {
    orientation_ = orientation;
  }

  const Eigen::Vector3f& GetPosition() {
    return position_;
  }
  
  void SetPosition(Eigen::Vector3f position){
    position_ = position;
  }

  const Eigen::MatrixXf& vertices() const {
    return vertices_;
  }

  const std::vector<GLuint>& indices() const {
    return indices_;
  }

  void SetVerticesIntoGpu() {
    SetVertexArrayObject(*this,
                         &vertex_buffer_object_id_,
                         &vertex_array_object_id_,
                         &element_buffer_object_id_);
    //std::cout << "Some object's VAO: " << vertex_array_object_id_ << std::endl;
  }

  void getIDs (GLuint * vbo_id, GLuint * vao_id, GLuint * ebo_id, GLuint * tex_id){
    vbo_id = &vertex_buffer_object_id_;
    vao_id = &vertex_array_object_id_;
    ebo_id = &element_buffer_object_id_;
    tex_id = &texture_id_;
    //std::cout << "\nReturnee: " << *vao_id << " \nAnd it should be: " << vertex_array_object_id_ << "\n\n";
  }
  
  GLuint getVAO() { return vertex_array_object_id_; }
  GLuint getTex() { /*std::cout << "Tex returned: " << texture_id_ << "\n\n";*/ return texture_id_; }
  
  void clean_up_after_yourself(){
    glDeleteVertexArrays(1, &vertex_array_object_id_);
    glDeleteBuffers(1, &vertex_buffer_object_id_);
  }
  
  bool no_rotate;
private:
  // Attributes.
  // The convention we will use is to define a '_' after the name
  // of the attribute.
  Eigen::Vector3f orientation_;
  Eigen::Vector3f position_;
  Eigen::MatrixXf vertices_;
  std::vector<GLuint> indices_;
  GLuint vertex_buffer_object_id_;
  GLuint vertex_array_object_id_;
  GLuint element_buffer_object_id_;
  GLuint texture_id_;
  const GLfloat rotation_speed = 50.0f;
};

#endif
